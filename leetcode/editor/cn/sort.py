def fast_sort(iterable):
    left = []
    right = []
    flag = None
    for i in iterable:
        if not flag:
            flag = i
            continue
        if i > flag:
            right.append(i)
        else:
            left.append(i)
    if len(left) > 1:
        sort_left = fast_sort(left)
    else:
        sort_left = left
    if len(right) > 1:
        sort_right = fast_sort(right)
    else:
        sort_right = right
    result = []
    result.extend(sort_left)
    result.append(flag)
    result.extend(sort_right)
    return result


a = [11, 5, 6, 7, 2, 5, 6, 87, 9, 5, 6, 7, 5, 32, ]
print(fast_sort(a))
