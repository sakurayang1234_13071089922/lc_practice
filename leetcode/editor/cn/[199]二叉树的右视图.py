# 给定一个二叉树的 根节点 root，想象自己站在它的右侧，按照从顶部到底部的顺序，返回从右侧所能看到的节点值。 
# 
#  
# 
#  示例 1: 
# 
#  
# 
#  
# 输入: [1,2,3,null,5,null,4]
# 输出: [1,3,4]
#  
# 
#  示例 2: 
# 
#  
# 输入: [1,null,3]
# 输出: [1,3]
#  
# 
#  示例 3: 
# 
#  
# 输入: []
# 输出: []
#  
# 
#  
# 
#  提示: 
# 
#  
#  二叉树的节点个数的范围是 [0,100] 
#  
#  -100 <= Node.val <= 100 
#  
# 
#  Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 983 👎 0
import copy
from collections import deque
# leetcode submit region begin(Prohibit modification and deletion)
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        tmp_node_list=[]
        tmp_node_list2=[]
        view_list=[]
        if not root:
            return view_list
        tmp_node_list.append(root)
        while 1:
            node_list=tmp_node_list if tmp_node_list else tmp_node_list2
            tmp_list=tmp_node_list if tmp_node_list2 else tmp_node_list2
            tmp_view_list=[]
            for node in node_list:
                if not node:
                    continue
                tmp_list.append(node.left)
                tmp_list.append(node.right)
                tmp_view_list.append(node.val)
            node_list.clear()
            if tmp_view_list:
                valid_view=tmp_view_list.pop()
                view_list.append(valid_view)
            if not (any(tmp_node_list) or any(tmp_node_list2)):
                break
        return view_list
        
                
            
                
        
# leetcode submit region end(Prohibit modification and deletion)
