# 找出所有相加之和为 n 的 k 个数的组合，且满足下列条件： 
# 
#  
#  只使用数字1到9 
#  每个数字 最多使用一次 
#  
# 
#  返回 所有可能的有效组合的列表 。该列表不能包含相同的组合两次，组合可以以任何顺序返回。 
# 
#  
# 
#  示例 1: 
# 
#  
# 输入: k = 3, n = 7
# 输出: [[1,2,4]]
# 解释:
# 1 + 2 + 4 = 7
# 没有其他符合的组合了。 
# 
#  示例 2: 
# 
#  
# 输入: k = 3, n = 9
# 输出: [[1,2,6], [1,3,5], [2,3,4]]
# 解释:
# 1 + 2 + 6 = 9
# 1 + 3 + 5 = 9
# 2 + 3 + 4 = 9
# 没有其他符合的组合了。 
# 
#  示例 3: 
# 
#  
# 输入: k = 4, n = 1
# 输出: []
# 解释: 不存在有效的组合。
# 在[1,9]范围内使用4个不同的数字，我们可以得到的最小和是1+2+3+4 = 10，因为10 > 1，没有有效的组合。
#  
# 
#  
# 
#  提示: 
# 
#  
#  2 <= k <= 9 
#  1 <= n <= 60 
#  
# 
#  Related Topics 数组 回溯 👍 779 👎 0
import copy

from typing import List
# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        nums_list=[9,8,7,6,5,4,3,2,1]
        def backtrace(v,nums_list):

            if v==0:
                if sum(nums) == n:
                    result.append(copy.deepcopy(nums))
                    return True
                
                elif sum(nums)>n:
                    return True

            else:
                for _ in range(len(nums_list)):
                    nums.append(nums_list.pop())
                    tmp_nums_list= copy.deepcopy(nums_list)
                    if backtrace(v-1,tmp_nums_list):
                        # print(result)
                        nums.pop()
                        # print(nums)
                        break
                    nums.pop()
                    
        nums=[]
        result=[]
        backtrace(k,nums_list)
        return result
        
# leetcode submit region end(Prohibit modification and deletion)

test=Solution()
# print(test.combinationSum3(3,9))
print(test.combinationSum3(3,7))