# 泰波那契序列 Tn 定义如下： 
# 
#  T0 = 0, T1 = 1, T2 = 1, 且在 n >= 0 的条件下 Tn+3 = Tn + Tn+1 + Tn+2 
# 
#  给你整数 n，请返回第 n 个泰波那契数 Tn 的值。 
# 
#  
# 
#  示例 1： 
# 
#  输入：n = 4
# 输出：4
# 解释：
# T_3 = 0 + 1 + 1 = 2
# T_4 = 1 + 1 + 2 = 4
#  
# 
#  示例 2： 
# 
#  输入：n = 25
# 输出：1389537
#  
# 
#  
# 
#  提示： 
# 
#  
#  0 <= n <= 37 
#  答案保证是一个 32 位整数，即 answer <= 2^31 - 1。 
#  
# 
#  Related Topics 记忆化搜索 数学 动态规划 👍 287 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def tribonacci(self, n: int) -> int:

        dp = [0, 1, 1]
        if n<=2:
            return dp[n]
        for i in range(3,n+1):
            dp.append(dp[i-3]+dp[i-2]+dp[i-1])
            
            
        

        
        return dp[-1]
    
    
    # def tribonacci(self, n: int) -> int:
    #     if n<3:
    #         dp=[0,1,1]
    #         return dp[n]
    #
    #     dp=[0]*(n+1)
    #     dp[1]=1
    #     dp[2]=1
    #     def dp_tribonacci(n):
    #         if n <=2:
    #             return dp[n]
    #         if dp[-1]!=0:
    #             return dp[n-1]
    #         dp[n]=dp_tribonacci(n-1)+dp_tribonacci(n-2)+dp_tribonacci(n-3)
    #         # print(f"dp {n} = {dp[n]}")
    #         return dp[n]
    #     return dp_tribonacci(n)
# leetcode submit region end(Prohibit modification and deletion)

test=Solution()
print(test.tribonacci(4))
print(test.tribonacci(25))