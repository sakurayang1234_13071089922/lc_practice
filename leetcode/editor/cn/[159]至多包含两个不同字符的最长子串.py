# Related Topics 哈希表 字符串 滑动窗口 👍 219 👎 0

def solution(s):
    char_set=set()
    char_count={}
    left,right=0,0
    max_length=0
    n=len(s)
    while right<n:

        current_char=s[right:right+1]
        if current_char not in char_set and len(char_set)==2:
            out_char = s[left:left + 1]
            out_char_count = char_count.get(out_char)
            if out_char_count == 1:
                del char_count[out_char]
                char_set.remove(out_char)
            else:
                char_count[out_char] = char_count[out_char] - 1
            max_length=max(max_length,right-left)
            left += 1
            continue
        char_set.add(current_char)
        current_char_count=char_count.get(current_char,None)
        char_count[current_char]=current_char_count+1 if current_char_count else 1
        right+=1
    max_length = max(max_length, right - left)

    return max_length


s="ccaabbb"
print(solution(s))