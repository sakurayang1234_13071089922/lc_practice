# 给定两个字符串 s 和 t ，如果它们的编辑距离为 1 ，则返回 true ，否则返回 false 。 
# 
#  字符串 s 和字符串 t 之间满足编辑距离等于 1 有三种可能的情形： 
# 
#  
#  往 s 中插入 恰好一个 字符得到 t 
#  从 s 中删除 恰好一个 字符得到 t 
#  在 s 中用 一个不同的字符 替换 恰好一个 字符得到 t 
#  
# 
#  
# 
#  示例 1： 
# 
#  
# 输入: s = "ab", t = "acb"
# 输出: true
# 解释: 可以将 'c' 插入字符串 s 来得到 t。
#  
# 
#  示例 2: 
# 
#  
# 输入: s = "cab", t = "ad"
# 输出: false
# 解释: 无法通过 1 步操作使 s 变为 t。 
# 
#  
# 
#  提示: 
# 
#  
#  0 <= s.length, t.length <= 10⁴ 
#  s 和 t 由小写字母，大写字母和数字组成 
#  
# 
#  Related Topics 双指针 字符串 👍 133 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def isOneEditDistance(self, s: str, t: str) -> bool:
        if s==t:
            return False
        s_len=len(s)
        t_len=len(t)
        if t_len==s_len:
            diff_count=0
            point=0
            while point<t_len:
                if diff_count>1:
                    return False
                if s[point:point+1]!=t[point:point+1]:
                    diff_count+=1
                    point+=1
                    continue
                point+=1
            return True
        short,long=0,0
        short_str,long_str=(s,t) if s_len<t_len else (t,s)
        diff_count=0
        while long<len(long_str):
            if diff_count>1:
                return False
            if short_str[short:short+1]==long_str[long:long+1]:
                short+=1
                long+=1
                continue
            diff_count+=1
            long+=1
            
        return True
        
        
# leetcode submit region end(Prohibit modification and deletion)
