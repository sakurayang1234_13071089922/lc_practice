# 给你二叉树的根节点 root ，返回它节点值的 前序 遍历。 
# 
#  
# 
#  示例 1： 
#  
#  
# 输入：root = [1,null,2,3]
# 输出：[1,2,3]
#  
# 
#  示例 2： 
# 
#  
# 输入：root = []
# 输出：[]
#  
# 
#  示例 3： 
# 
#  
# 输入：root = [1]
# 输出：[1]
#  
# 
#  示例 4： 
#  
#  
# 输入：root = [1,2]
# 输出：[1,2]
#  
# 
#  示例 5： 
#  
#  
# 输入：root = [1,null,2]
# 输出：[1,2]
#  
# 
#  
# 
#  提示： 
# 
#  
#  树中节点数目在范围 [0, 100] 内 
#  -100 <= Node.val <= 100 
#  
# 
#  
# 
#  进阶：递归算法很简单，你可以通过迭代算法完成吗？ 
# 
#  Related Topics 栈 树 深度优先搜索 二叉树 👍 928 👎 0


from typing import Optional,List
# leetcode submit region begin(Prohibit modification and deletion)
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def preorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        # def add_val(root):
        #     if not root:
        #         return
        #     ret.append(root.val)
        #     add_val(root.left)
        #     add_val(root.right)
        # ret = []
        # add_val(root)
        # return ret
        
        stack=[]
        ret=[]
        tmp_node=root
        while stack or tmp_node:
            while tmp_node:
                ret.append(tmp_node.val)
                stack.append(tmp_node)
                tmp_node=tmp_node.left
            tmp_node=stack.pop()
            tmp_node=tmp_node.right
        return ret
    
        
# leetcode submit region end(Prohibit modification and deletion)
