# 给你一个下标从 0 开始、大小为 n x n 的整数矩阵 grid ，返回满足 Ri 行和 Cj 列相等的行列对 (Ri, Cj) 的数目。 
# 
#  如果行和列以相同的顺序包含相同的元素（即相等的数组），则认为二者是相等的。 
# 
#  
# 
#  示例 1： 
# 
#  
# 
#  
# 输入：grid = [[3,2,1],[1,7,6],[2,7,7]]
# 输出：1
# 解释：存在一对相等行列对：
# - (第 2 行，第 1 列)：[2,7,7]
#  
# 
#  示例 2： 
# 
#  
# 
#  
# 输入：grid = [[3,1,2,2],[1,4,4,5],[2,4,2,2],[2,4,2,2]]
# 输出：3
# 解释：存在三对相等行列对：
# - (第 0 行，第 0 列)：[3,1,2,2]
# - (第 2 行, 第 2 列)：[2,4,2,2]
# - (第 3 行, 第 2 列)：[2,4,2,2]
#  
# 
#  
# 
#  提示： 
# 
#  
#  n == grid.length == grid[i].length 
#  1 <= n <= 200 
#  1 <= grid[i][j] <= 10⁵ 
#  
# 
#  Related Topics 数组 哈希表 矩阵 模拟 👍 84 👎 0

from typing import List
# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def equalPairs(self, grid: List[List[int]]) -> int:
        n=len(grid)
        pairs_dict={}
        column_pairs_list=[list() for i in range(n)]
        result=0
        for i in grid:
            i_str=[]
            for index,j in enumerate(i):
                j=str(j)
                column_pairs_list[index].append(j)
                i_str.append(j)
            pair=".".join(i_str)
            if not pair in pairs_dict:
                pairs_dict[pair]=1
            else:
                pairs_dict[pair]=pairs_dict[pair]+1
        column_pairs_dict={}
        for i in column_pairs_list:
            pair='.'.join(i)
            if pair in column_pairs_dict:
                column_pairs_dict[pair]= column_pairs_dict[pair]+1
            else:
                column_pairs_dict[pair]=1
        for pair,_count in pairs_dict.items():
            if pair in column_pairs_dict:
                result+=_count*column_pairs_dict[pair]
        return result
            
        
        
        
# leetcode submit region end(Prohibit modification and deletion)
test=Solution()
print(test.equalPairs([[3, 2, 1], [1, 7, 6], [2, 7, 7]]))
print(test.equalPairs([[13,13], [13,13]]))