# 给你一个的整数数组 nums, 将该数组重新排序后使 nums[0] <= nums[1] >= nums[2] <= nums[3]... 
# 
#  输入数组总是有一个有效的答案。 
# 
#  
# 
#  示例 1: 
# 
#  
# 输入：nums = [3,5,2,1,6,4]
# 输出：[3,5,1,6,2,4]
# 解释：[1,6,2,5,3,4]也是有效的答案 
# 
#  示例 2: 
# 
#  
# 输入：nums = [6,6,5,6,3,8]
# 输出：[6,6,5,6,3,8]
#  
# 
#  
# 
#  提示： 
# 
#  
#  
# 
#  
#  1 <= nums.length <= 5 * 10⁴ 
#  0 <= nums[i] <= 10⁴ 
#  输入的 nums 保证至少有一个答案。 
#  
# 
#  
# 
#  进阶：你能在 O(n) 时间复杂度下解决这个问题吗？ 
# 
#  Related Topics 贪心 数组 排序 👍 133 👎 0

from typing import List
# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        
        n=len(nums)
        i=0
        while i<n:
            if i+1<n:
                if nums[i] > nums[i + 1]:
                    nums[i], nums[i + 1] = nums[i + 1], nums[i]
            if i+2<n:
                if nums[i+1]<nums[i+2]:
                    nums[i+1],nums[i+2]=nums[i+2],nums[i+1]
            i+=2
        return
# leetcode submit region end(Prohibit modification and deletion)
