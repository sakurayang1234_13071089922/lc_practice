# 给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。 
# 
#  元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现不止一次。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：s = "hello"
# 输出："holle"
#  
# 
#  示例 2： 
# 
#  
# 输入：s = "leetcode"
# 输出："leotcede" 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= s.length <= 3 * 10⁵ 
#  s 由 可打印的 ASCII 字符组成 
#  
# 
#  Related Topics 双指针 字符串 👍 329 👎 0

class Solution:
    def reverseVowels(self, s: str) -> str:
        #暴力解法
        not_vowels = []
        vowels = []
        default_vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
        for i in s:
            if i in default_vowels:
                vowels.append(i)
                not_vowels.append(None)
            else:
                not_vowels.append(i)
        result_list = []
        vowels.reverse()
        vowerl_count = 0
        for i, v in enumerate(not_vowels):
            if not v:
                result_list.append(vowels[vowerl_count])
                vowerl_count += 1
            else:
                result_list.append(v)
        return ''.join(result_list)

# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def reverseVowels(self, s: str) -> str:
        #双指针
        length=len(s)
        left=0
        right=length-1
        vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
        s_list=[i for i in s]
        while left<=right:
            if s_list[left] not in vowels:
                left+=1
                continue
            if s_list[right] not in vowels:
                right-=1
                continue
            temp=s_list[left]
            s_list[left]=s_list[right]
            s_list[right]=temp
            left+=1
            right-=1
        return ''.join(s_list)
            
# leetcode submit region end(Prohibit modification and deletion)

test=Solution()
print(test.reverseVowels("leetcode"))