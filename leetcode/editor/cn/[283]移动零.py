# 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。 
# 
#  请注意 ，必须在不复制数组的情况下原地对数组进行操作。 
# 
#  
# 
#  示例 1: 
# 
#  
# 输入: nums = [0,1,0,3,12]
# 输出: [1,3,12,0,0]
#  
# 
#  示例 2: 
# 
#  
# 输入: nums = [0]
# 输出: [0] 
# 
#  
# 
#  提示: 
#  
# 
#  
#  1 <= nums.length <= 10⁴ 
#  -2³¹ <= nums[i] <= 2³¹ - 1 
#  
# 
#  
# 
#  进阶：你能尽量减少完成的操作次数吗？ 
#  Related Topics 数组 双指针 👍 1512 👎 0
from typing import List

# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        zero_index=[]
        count_zero=0
        for i,v in enumerate(nums):
            if v ==0:
                count_zero+=1
                zero_index.append(i)

        for i,index_value in enumerate(zero_index):
            nums.pop(index_value-i)
        for i in range(count_zero):
            nums.append(0)

        """
        Do not return anything, modify nums in-place instead.
        """
# leetcode submit region end(Prohibit modification and deletion)

    def moveZeroes2(self, nums: List[int]) -> None:
        n = len(nums)
        left = right = 0
        while right < n:
            if nums[right] != 0:
                nums[left], nums[right] = nums[right], nums[left]
                left += 1
            right += 1
    
    def moveZeroes3(self, nums: List[int]) -> None:
        n = len(nums)
        left = right = 0
        while right < n:
            if nums[left] == 0 and nums[right] != 0:
                nums[left], nums[right] = nums[right], nums[left]
                left += 1
                right += 1
                continue
            if nums[left] != 0:
                left += 1
                right += 1
                continue
            right += 1

if __name__=="__main__":
    solution=Solution()
    # nums = [1, 2, 3, 0]
    nums = [1, 2, 0, 3, 0]
    # solution.moveZeroes2(nums)
    solution.moveZeroes3(nums)
    print(nums)