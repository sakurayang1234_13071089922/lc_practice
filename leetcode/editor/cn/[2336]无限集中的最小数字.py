# 现有一个包含所有正整数的集合 [1, 2, 3, 4, 5, ...] 。 
# 
#  实现 SmallestInfiniteSet 类： 
# 
#  
#  SmallestInfiniteSet() 初始化 SmallestInfiniteSet 对象以包含 所有 正整数。 
#  int popSmallest() 移除 并返回该无限集中的最小整数。 
#  void addBack(int num) 如果正整数 num 不 存在于无限集中，则将一个 num 添加 到该无限集中。 
#  
# 
#  
# 
#  示例： 
# 
#  输入
# ["SmallestInfiniteSet", "addBack", "popSmallest", "popSmallest", 
# "popSmallest", "addBack", "popSmallest", "popSmallest", "popSmallest"]
# [[], [2], [], [], [], [1], [], [], []]
# 输出
# [null, null, 1, 2, 3, null, 1, 4, 5]
# 
# 解释
# SmallestInfiniteSet smallestInfiniteSet = new SmallestInfiniteSet();
# smallestInfiniteSet.addBack(2);    // 2 已经在集合中，所以不做任何变更。
# smallestInfiniteSet.popSmallest(); // 返回 1 ，因为 1 是最小的整数，并将其从集合中移除。
# smallestInfiniteSet.popSmallest(); // 返回 2 ，并将其从集合中移除。
# smallestInfiniteSet.popSmallest(); // 返回 3 ，并将其从集合中移除。
# smallestInfiniteSet.addBack(1);    // 将 1 添加到该集合中。
# smallestInfiniteSet.popSmallest(); // 返回 1 ，因为 1 在上一步中被添加到集合中，
#                                    // 且 1 是最小的整数，并将其从集合中移除。
# smallestInfiniteSet.popSmallest(); // 返回 4 ，并将其从集合中移除。
# smallestInfiniteSet.popSmallest(); // 返回 5 ，并将其从集合中移除。 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= num <= 1000 
#  最多调用 popSmallest 和 addBack 方法 共计 1000 次 
#  
# 
#  Related Topics 设计 哈希表 堆（优先队列） 👍 38 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
class SmallestInfiniteSet:

    def __init__(self):
        self.pop_num=set()


    def popSmallest(self) -> int:
        if not self.pop_num:
            self.pop_num.add(1)
            return 1
        start=min(self.pop_num)
        if start>1:
            for i in range(1,start):
                self.pop_num.add(i)
                return i
                
        while 1:
            start+=1
            if start not in self.pop_num:
                self.pop_num.add(start)
                return start
        

    def addBack(self, num: int) -> None:
        if num in self.pop_num:
            self.pop_num.remove(num)



# Your SmallestInfiniteSet object will be instantiated and called as such:
obj = SmallestInfiniteSet()
param_1 = obj.popSmallest()
print(param_1)
obj.addBack(5)
print(obj.popSmallest())
print(obj.popSmallest())
print(obj.popSmallest())
# leetcode submit region end(Prohibit modification and deletion)
