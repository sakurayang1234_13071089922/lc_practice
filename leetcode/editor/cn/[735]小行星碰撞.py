# 给定一个整数数组 asteroids，表示在同一行的小行星。 
# 
#  对于数组中的每一个元素，其绝对值表示小行星的大小，正负表示小行星的移动方向（正表示向右移动，负表示向左移动）。每一颗小行星以相同的速度移动。 
# 
#  找出碰撞后剩下的所有小行星。碰撞规则：两个小行星相互碰撞，较小的小行星会爆炸。如果两颗小行星大小相同，则两颗小行星都会爆炸。两颗移动方向相同的小行星，永远
# 不会发生碰撞。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：asteroids = [5,10,-5]
# 输出：[5,10]
# 解释：10 和 -5 碰撞后只剩下 10 。 5 和 10 永远不会发生碰撞。 
# 
#  示例 2： 
# 
#  
# 输入：asteroids = [8,-8]
# 输出：[]
# 解释：8 和 -8 碰撞后，两者都发生爆炸。 
# 
#  示例 3： 
# 
#  
# 输入：asteroids = [10,2,-5]
# 输出：[10]
# 解释：2 和 -5 发生碰撞后剩下 -5 。10 和 -5 发生碰撞后剩下 10 。 
# 
#  
# 
#  提示： 
# 
#  
#  2 <= asteroids.length <= 10⁴ 
#  -1000 <= asteroids[i] <= 1000 
#  asteroids[i] != 0 
#  
# 
#  Related Topics 栈 数组 模拟 👍 452 👎 0
from typing import List

# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        tmp_stack=[]
        while asteroids:
            new_asteroid=asteroids.pop()
            if not asteroids:
                asteroids.append(new_asteroid)
                tmp_stack.reverse()
                asteroids.extend(tmp_stack)
                return asteroids
            last_asteroid=asteroids.pop()
            if new_asteroid*last_asteroid>0 or last_asteroid<0:
                tmp_stack.append(new_asteroid)
                asteroids.append(last_asteroid)
                continue
            if abs(new_asteroid)==abs(last_asteroid):
                tmp_stack.reverse()
                asteroids.extend(tmp_stack)
                tmp_stack=[]
                continue
            elif abs(new_asteroid)>abs(last_asteroid):
                asteroids.append(new_asteroid)
            else:
                asteroids.append(last_asteroid)
                tmp_stack.reverse()
                
                asteroids.extend(tmp_stack)
                tmp_stack=[]
        if tmp_stack:
            asteroids.extend(tmp_stack)
        return asteroids



# leetcode submit region end(Prohibit modification and deletion)

test=Solution()
print(test.asteroidCollision([-2, -1, 1, 2]))
print(test.asteroidCollision([10, 2, -5]))
print(test.asteroidCollision([8, -8]))
print(test.asteroidCollision([5, 10, -5]))