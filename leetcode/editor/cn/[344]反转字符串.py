# 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。 
# 
#  不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：s = ["h","e","l","l","o"]
# 输出：["o","l","l","e","h"]
#  
# 
#  示例 2： 
# 
#  
# 输入：s = ["H","a","n","n","a","h"]
# 输出：["h","a","n","n","a","H"] 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= s.length <= 10⁵ 
#  s[i] 都是 ASCII 码表中的可打印字符 
#  
#  Related Topics 递归 双指针 字符串 👍 551 👎 0

from typing import List

# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        """
            解法一，遍历列表，对调对应位置上的两个数字，游标到达数组中间停止
        """
        # length=len(s)
        # for i,v in enumerate(s):
        #     temp=v
        #     if i >= length//2:
        #         break
        #     s[i]=s[length-i-1]
        #     s[length-i-1]=temp
        #
        # print(s)
        """
            解法二，双指针
        """

        front=0
        back=len(s)-1
        while front<back:
            s[front],s[back]=s[back],s[front]
            front+=1
            back-=1
        print(s)



# leetcode submit region end(Prohibit modification and deletion)

if __name__=="__main__":
    a=Solution()
    s=['a','b','c','d','e']
    a.reverseString(s)
