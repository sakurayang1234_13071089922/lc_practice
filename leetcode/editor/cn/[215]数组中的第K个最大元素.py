# 给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。 
# 
#  请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。 
# 
#  你必须设计并实现时间复杂度为 O(n) 的算法解决此问题。 
# 
#  
# 
#  示例 1: 
# 
#  
# 输入: [3,2,1,5,6,4], k = 2
# 输出: 5
#  
# 
#  示例 2: 
# 
#  
# 输入: [3,2,3,1,2,4,5,5,6], k = 4
# 输出: 4 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= k <= nums.length <= 10⁵ 
#  -10⁴ <= nums[i] <= 10⁴ 
#  
# 
#  Related Topics 数组 分治 快速选择 排序 堆（优先队列） 👍 2342 👎 0


# leetcode submit region begin(Prohibit modification and deletion)

class maxheap():
    def __init__(self, iterable):
        self.maxheaq = [i for i in iterable]
        self.valid()
    
    def insert(self, val):
        self.maxheaq.append(val)
        new_val_index = len(self.maxheaq) - 1
        if new_val_index % 2 == 0:
            parent_index = (new_val_index - 1) // 2
        else:
            parent_index = new_val_index // 2
        while self.maxheaq[parent_index] < self.maxheaq[new_val_index]:
            self.maxheaq[parent_index], self.maxheaq[new_val_index] = self.maxheaq[new_val_index], self.maxheaq[
                parent_index]
            if parent_index == 0:
                break
            new_val_index = parent_index
            if new_val_index % 2 == 0:
                parent_index = (new_val_index - 1) // 2
            else:
                parent_index = new_val_index // 2
    
    def delete_pop(self):
        self.delete(self.maxheaq[0])
    
    def delete(self, val):
        need_delete_index = -1
        for index_, v in enumerate(self.maxheaq):
            if v == val:
                need_delete_index = index_
                break
        if need_delete_index < 0:
            return
        elif need_delete_index == (len(self.maxheaq) - 1):
            self.maxheaq.pop()
            return
        left_index = need_delete_index * 2 + 1
        right_index = need_delete_index * 2 + 2
        if (right_index >= len(self.maxheaq)) or (left_index >= len(self.maxheaq)):
            self.maxheaq[need_delete_index] = self.maxheaq.pop()
            return
        elif self.maxheaq[left_index] < self.maxheaq[right_index]:
            fix_index = right_index
        else:
            fix_index = left_index
        self.maxheaq[need_delete_index] = self.maxheaq[fix_index]
        last_val = self.maxheaq.pop()
        self.maxheaq[fix_index] = last_val
        if last_val >= self.maxheaq[need_delete_index]:
            self.maxheaq[need_delete_index], self.maxheaq[fix_index] = self.maxheaq[fix_index], self.maxheaq[
                need_delete_index]
            return
        while fix_index < len(self.maxheaq):
            left_index = fix_index * 2 + 1
            right_index = fix_index * 2 + 2
            if (left_index >= len(self.maxheaq)) or (right_index >= len(self.maxheaq)):
                break
            if self.maxheaq[left_index] < self.maxheaq[right_index]:
                self.maxheaq[fix_index], self.maxheaq[right_index] = self.maxheaq[right_index], self.maxheaq[fix_index]
                fix_index = right_index
            elif self.maxheaq[left_index] > self.maxheaq[right_index]:
                self.maxheaq[fix_index], self.maxheaq[left_index] = self.maxheaq[left_index], self.maxheaq[fix_index]
                fix_index = left_index
            else:
                break
    
    def head(self):
        return self.maxheaq[0]
    
    def valid(self):
        n = len(self.maxheaq)
        for i in range(n):
            root_index = 0
            flag = False
            while root_index < n:
                
                left_index = root_index * 2 + 1
                right_index = root_index * 2 + 2
                
                if left_index < n:
                    if self.maxheaq[left_index] > self.maxheaq[root_index]:
                        self.maxheaq[root_index], self.maxheaq[left_index] = self.maxheaq[left_index], self.maxheaq[
                            root_index]
                        flag = True
                if right_index < n:
                    if self.maxheaq[right_index] > self.maxheaq[root_index]:
                        self.maxheaq[root_index], self.maxheaq[right_index] = self.maxheaq[right_index], self.maxheaq[
                            root_index]
                        flag = True
                
                root_index += 1
            if not flag:
                break


from typing import List


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heaq = maxheap(nums)
        for i in range(k - 1):
            heaq.delete_pop()
        return heaq.head()


# leetcode submit region end(Prohibit modification and deletion)


test = Solution()
print(test.findKthLargest([-1, 2, 0], 2))
