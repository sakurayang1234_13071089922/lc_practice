# 给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入: s = "leetcode"
# 输出: 0
#  
# 
#  示例 2: 
# 
#  
# 输入: s = "loveleetcode"
# 输出: 2
#  
# 
#  示例 3: 
# 
#  
# 输入: s = "aabb"
# 输出: -1
#  
# 
#  
# 
#  提示: 
# 
#  
#  1 <= s.length <= 10⁵ 
#  s 只包含小写字母 
#  
#  Related Topics 队列 哈希表 字符串 计数 👍 532 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def firstUniqChar(self, s: str) -> int:
        """
            优化，第二次遍历不再遍历字符串s，而是直接遍历字段dict_
        """
        dict_={}
        index=0
        recount=set()
        for i in s:
            index+=1
            if i in recount:
                continue
            if i in dict_:
                dict_.pop(i)
                recount.add(i)
                continue
            else:
                dict_[i]=index-1
        if len(dict_)==0:
            return -1
        result=len(s)
        for i in dict_:
            if dict_[i]<result:
                result=dict_[i]
        return result
        # dict_={}
        # index=0
        # recount=set()
        # for i in s:
        #     index+=1
        #     if i in recount:
        #         continue
        #     if i in dict_:
        #         dict_.pop(i)
        #         recount.add(i)
        #         continue
        #     else:
        #         dict_[i]=index-1
        # if len(dict_)==0:
        #     return -1
        # for i in s:
        #     if i in dict_:
        #         return dict_[i]



# leetcode submit region end(Prohibit modification and deletion)


if __name__ == '__main__':
    s="leetcode"
    a=Solution()
    print(a.firstUniqChar(s))