# 写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项（即 F(N)）。斐波那契数列的定义如下： 
# 
#  
# F(0) = 0,   F(1) = 1
# F(N) = F(N - 1) + F(N - 2), 其中 N > 1. 
# 
#  斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。 
# 
#  答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：n = 2
# 输出：1
#  
# 
#  示例 2： 
# 
#  
# 输入：n = 5
# 输出：5
#  
# 
#  
# 
#  提示： 
# 
#  
#  0 <= n <= 100 
#  
# 
#  Related Topics 记忆化搜索 数学 动态规划 👍 392 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    # dp = {
    #     0: 0,
    #     1: 1
    # }
    def fib(self, n: int) -> int:
        # if n <= 1:
        #     return n
        # return (self.fib(n-1)+self.fib(n-2))% 1000000007

        # if n <=1:
        #     return n
        # if self.dp.get(n-1):
        #     fib_n_1= self.dp.get(n - 1)
        # else:
        #     fib_n_1=self.fib(n-1)
        #     self.dp[n-1]=fib_n_1
        # if self.dp.get(n-2):
        #     fib_n_2=self.dp.get(n-2)
        # else:
        #     fib_n_2=self.fib(n-2)
        #     self.dp[n-2]=fib_n_2
        # return (fib_n_1+fib_n_2) % 1000000007

        if n<=1:
            return n
        dp=[0]*n
        dp[1]=1
        for i in range(2,n):
            dp[i]=dp[i-1]+dp[i-2]
        return (dp[-1]+dp[-2])% 1000000007
        # if n<=1:
        #     return n
        # a,b=0,1
        # for i in range(2,n+1):
        #     a,b=b,a+b
        # return (b)% 1000000007




# leetcode submit region end(Prohibit modification and deletion)

a=Solution()
print(a.fib(5))