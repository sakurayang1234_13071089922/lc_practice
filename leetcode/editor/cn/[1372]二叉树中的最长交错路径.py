# 给你一棵以 root 为根的二叉树，二叉树中的交错路径定义如下： 
# 
#  
#  选择二叉树中 任意 节点和一个方向（左或者右）。 
#  如果前进方向为右，那么移动到当前节点的的右子节点，否则移动到它的左子节点。 
#  改变前进方向：左变右或者右变左。 
#  重复第二步和第三步，直到你在树中无法继续移动。 
#  
# 
#  交错路径的长度定义为：访问过的节点数目 - 1（单个节点的路径长度为 0 ）。 
# 
#  请你返回给定树中最长 交错路径 的长度。 
# 
#  
# 
#  示例 1： 
# 
#  
# 
#  输入：root = [1,null,1,1,1,null,null,1,1,null,1,null,null,null,1,null,1]
# 输出：3
# 解释：蓝色节点为树中最长交错路径（右 -> 左 -> 右）。
#  
# 
#  示例 2： 
# 
#  
# 
#  输入：root = [1,1,1,null,1,null,null,1,1,null,1]
# 输出：4
# 解释：蓝色节点为树中最长交错路径（左 -> 右 -> 左 -> 右）。
#  
# 
#  示例 3： 
# 
#  输入：root = [1]
# 输出：0
#  
# 
#  
# 
#  提示： 
# 
#  
#  每棵树最多有 50000 个节点。 
#  每个节点的值在 [1, 100] 之间。 
#  
# 
#  Related Topics 树 深度优先搜索 动态规划 二叉树 👍 153 👎 0
from typing import Optional


# leetcode submit region begin(Prohibit modification and deletion)
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None,flag=None):
        self.val = val
        self.left = left
        self.right = right
        self.flag=flag


class Solution:
    # def longestZigZag(self, root: Optional[TreeNode]) -> int:
    #     def dsf(node: Optional[TreeNode], choice_left: bool, current_zig_zag):
    #         if not node:
    #             return 0
    #         if not (node.left or node.right):
    #             return current_zig_zag
    #         if choice_left and node.right:
    #             current_zig_zag += 1
    #             next_node = node.right
    #             last_left_choice = False
    #         elif (not choice_left) and node.left:
    #             current_zig_zag += 1
    #             next_node = node.left
    #             last_left_choice = True
    #         else:
    #             next_start= dsf(node, not choice_left, 0)
    #             return max(current_zig_zag, next_start)
    #
    #         left_value= dsf(next_node, last_left_choice, current_zig_zag)
    #         right_value=dsf(next_node,not last_left_choice,0)
    #         return max(current_zig_zag, left_value,right_value)
    #
    #     left_start= dsf(root, choice_left=True, current_zig_zag=0)
    #     right_start= dsf(root, choice_left=False, current_zig_zag=0)
    #     return max(left_start, right_start)
    
    def longestZigZag(self, root: Optional[TreeNode]) -> int:
        def dsf(node: Optional[TreeNode], choice_left: bool, current_zig_zag):
            if not node:
                return 0
            if not choice_left:
                left_start=dsf(node.left,True,current_zig_zag+1)
                right_start=dsf(node.right,False,1)
            else:
                left_start=dsf(node.left,False,1)
                right_start=dsf(node.right,True,current_zig_zag+1)
            return max(current_zig_zag, left_start, right_start)
        
        left_start = dsf(root, choice_left=True, current_zig_zag=0)
        right_start = dsf(root, choice_left=False, current_zig_zag=0)
        return max(left_start, right_start)


# leetcode submit region end(Prohibit modification and deletion)
#  输入：root = [1,null,1,1,1,null,null,1,1,null,1,null,null,null,1,null,1]

# _root = TreeNode(
#     val=1,
#     left=None,
#     right=TreeNode(
#         val=1,
#         left=TreeNode(
#             val=1,
#             left=None,
#             right=None),
#         right=TreeNode(
#             val=1,
#             left=TreeNode(
#                 val=1,
#                 left=None,
#                 right=TreeNode(
#                      val=1,
#                      left=None,
#                      right=TreeNode(
#                          val=1,
#                          left=None,
#                          right=TreeNode(
#                              val=1,
#                              left=None,
#                              right=None))
#                 )
#             ),
#             right=TreeNode(
#                 val=1,
#                 left=None,
#                 right=None
#             )
#         )
#     )
# )
_root=TreeNode(
    val=1,
    flag=1,
    left=TreeNode(
        val=1,flag=2,
        left=TreeNode(
            val=1,flag=3,
            left=None,
            right=None
        ),
        right=None
    ),
    right=TreeNode(
        val=1,flag=4,
        left=TreeNode(
            val=1,flag=5,
            left=TreeNode(
                val=1,flag=6,
                left=None,
                right=TreeNode(
                    val=1,flag=7,
                    left=None,
                    right=None
                )
            ),
            right=None
        ),
        right=TreeNode(
            val=1,flag=8,
            left=TreeNode(
                val=1,left=None,flag=9,
                right=TreeNode(val=1,left=None,right=None,flag=10)
            ),
            right=None
        )
    )
)
test = Solution()
print(test.longestZigZag(_root))
