# 给你一个字符数组 s ，反转其中 单词 的顺序。 
# 
#  单词 的定义为：单词是一个由非空格字符组成的序列。s 中的单词将会由单个空格分隔。 
# 
#  
#  
#  必须设计并实现 原地 解法来解决此问题，即不分配额外的空间。 
#  
#  
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：s = ["t","h","e"," ","s","k","y"," ","i","s"," ","b","l","u","e"]
# 输出：["b","l","u","e"," ","i","s"," ","s","k","y"," ","t","h","e"]
#  
# 
#  示例 2： 
# 
#  
# 输入：s = ["a"]
# 输出：["a"]
#  
# 
#  
# 
#  提示： 
# 
#  
#  1 <= s.length <= 10⁵ 
#  s[i] 可以是一个英文字母（大写或小写）、数字、或是空格 ' ' 。 
#  s 中至少存在一个单词 
#  s 不含前导或尾随空格 
#  题目数据保证：s 中的每个单词都由单个空格分隔 
#  
# 
#  Related Topics 双指针 字符串 👍 115 👎 0


from  typing import List
# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def reverseWords(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        left,right=0,len(s)-1
        while left<right:
            s[left],s[right]=s[right],s[left]
            left+=1
            right-=1
        left=0
        count=0
        print(s)
        while left<len(s):
            if s[left]!=" ":
                count+=1
                left+=1
                continue
            tmp_right=left-1
            tmp_left=tmp_right-count+1
            while tmp_left<tmp_right:
                s[tmp_left],s[tmp_right]=s[tmp_right],s[tmp_left]
                tmp_right-=1
                tmp_left+=1
            left+=1
            count=0
        if count:
            tmp_right = left - 1
            tmp_left = tmp_right - count + 1
            while tmp_left < tmp_right:
                s[tmp_left], s[tmp_right] = s[tmp_right], s[tmp_left]
                tmp_right -= 1
                tmp_left += 1
        return s
        
# leetcode submit region end(Prohibit modification and deletion)

s = ["t", "h", "e", " ", "s", "k", "y", " ", "i", "s", " ", "b", "l", "u", "e"]
test=Solution()
print(test.reverseWords(s))