# 给你一个二进制数组 nums ，你需要从中删掉一个元素。 
# 
#  请你在删掉元素的结果数组中，返回最长的且只包含 1 的非空子数组的长度。 
# 
#  如果不存在这样的子数组，请返回 0 。 
# 
#  
# 
#  提示 1： 
# 
#  
# 输入：nums = [1,1,0,1]
# 输出：3
# 解释：删掉位置 2 的数后，[1,1,1] 包含 3 个 1 。 
# 
#  示例 2： 
# 
#  
# 输入：nums = [0,1,1,1,0,1,1,0,1]
# 输出：5
# 解释：删掉位置 4 的数字后，[0,1,1,1,1,1,0,1] 的最长全 1 子数组为 [1,1,1,1,1] 。 
# 
#  示例 3： 
# 
#  
# 输入：nums = [1,1,1]
# 输出：2
# 解释：你必须要删除一个元素。 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= nums.length <= 10⁵ 
#  nums[i] 要么是 0 要么是 1 。 
#  
# 
#  Related Topics 数组 动态规划 滑动窗口 👍 112 👎 0
from typing import List

# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def longestSubarray(self, nums: List[int]) -> int:
        zero_flag=False
        part_one_lengths=[]
        n=len(nums)
        tmp_part_length=0
        last_num_zero=False
        for i in range(n):
            if i==n-1:
                if nums[i]==0:
                    part_one_lengths.append(tmp_part_length)
                    continue
                tmp_part_length+=1
                part_one_lengths.append(tmp_part_length)
                continue
            # if nums[i]==0 and zero_flag is False:
            #     continue
            zero_flag=True
            if nums[i]==1:
                tmp_part_length+=1
                last_num_zero=False
                continue
            if nums[i]==0:
                if last_num_zero is False:
                    part_one_lengths.append(tmp_part_length)
                    tmp_part_length=0
                    last_num_zero=True
                else:
                    part_one_lengths.append(0)
                    tmp_part_length=0
                    last_num_zero=True
        print(part_one_lengths)
        print(zero_flag)
        if len(part_one_lengths)==0:
            return 0
        if len(part_one_lengths)==1:
            
            return part_one_lengths[0]-1 if zero_flag else 0
        n_lengths=len(part_one_lengths)
        max_substr_length=0
        for i in range(n_lengths-1):
            if part_one_lengths[i+1]!=0:
                max_substr_length=max(max_substr_length,part_one_lengths[i]+part_one_lengths[i+1])
            else:
                max_substr_length=max(max_substr_length,part_one_lengths[i])
        return max_substr_length
        
            
# leetcode submit region end(Prohibit modification and deletion)


test=Solution()
# print(test.longestSubarray([1, 1, 0, 1]))
# print(test.longestSubarray([0, 1, 1, 1, 0, 1, 1, 0, 1]))
print(test.longestSubarray([1, 1, 1]))
# print(test.longestSubarray([1]))
print(test.longestSubarray([0,0,0,0]))
print(test.longestSubarray([0,0,1,1]))
# print(test.longestSubarray([1,1,1,0,0,1,0,1]))
# print(test.longestSubarray([0,0,0,1,1,1,0,0,1,0,1]))
# print(test.longestSubarray([0,0,0,1,1,1,0,0,1,0,1,0,0]))
# print(test.longestSubarray([0,0,0,1,1,1,0,0,1,0,1,0]))