# Related Topics 贪心 数组 👍 97 👎 0
import math


def solution(arrays:list[list[int]]):
    min_start=math.inf
    max_end=-math.inf
    min_start_index=None
    max_end_index=None
    for _index,i in enumerate(arrays):
        if i[0]<=min_start:
            min_start=i[0]
            min_start_index=_index
        if i[-1]>=max_end:
            max_end=i[-1]
            max_end_index=_index

    if min_start_index!=max_end_index:
        return max_end-min_start
    tmp_max_end=-math.inf
    tmp_min_start=math.inf
    for _index,i in enumerate(arrays):
        if _index==min_start_index:
            continue
        tmp_min_start=min(tmp_min_start,i[0])
        tmp_max_end=max(tmp_max_end,i[-1])
    print(tmp_min_start,tmp_max_end)
    print(min_start,max_end)
    return max(tmp_max_end-min_start,max_end-tmp_min_start)
    


# a=[[1,4],[0,5]]
# a=[[1],[1]]
a=[[1,5],[3,4]]
print(solution(a))