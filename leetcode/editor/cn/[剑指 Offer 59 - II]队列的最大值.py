# 请定义一个队列并实现函数 max_value 得到队列里的最大值，要求函数max_value、push_back 和 pop_front 的均摊时间复杂度都
# 是O(1)。 
# 
#  若队列为空，pop_front 和 max_value 需要返回 -1 
# 
#  示例 1： 
# 
#  输入: 
# ["MaxQueue","push_back","push_back","max_value","pop_front","max_value"]
# [[],[1],[2],[],[],[]]
# 输出: [null,null,null,2,1,2]
#  
# 
#  示例 2： 
# 
#  输入: 
# ["MaxQueue","pop_front","max_value"]
# [[],[],[]]
# 输出: [null,-1,-1]
#  
# 
#  
# 
#  限制： 
# 
#  
#  1 <= push_back,pop_front,max_value的总操作数 <= 10000 
#  1 <= value <= 10^5 
#  
# 
#  Related Topics 设计 队列 单调队列 👍 394 👎 0

from collections import deque



# leetcode submit region begin(Prohibit modification and deletion)
class MaxQueue:

    def __init__(self):
        self.data_deque=deque()
        self.max_deque=deque()

    def max_value(self) -> int:
        if self.max_deque:
            return self.max_deque[-1]
        return -1

    def push_back(self, value: int) -> None:
        self.data_deque.appendleft(value)
        if not self.max_deque:
            self.max_deque.appendleft(value)
            return
        while 1:
            tail=self.max_deque.popleft()
            if tail>value:
                self.max_deque.appendleft(tail)
                self.max_deque.appendleft(value)
                break
            if not self.max_deque:
                self.max_deque.appendleft(value)
                break

    def pop_front(self) -> int:
        if not self.data_deque:
            return -1

        result=self.data_deque.pop()
        if result ==self.max_deque[-1]:
            self.max_deque.pop()
        return result



# Your MaxQueue object will be instantiated and called as such:
# obj = MaxQueue()
# param_1 = obj.max_value()
# obj.push_back(value)
# param_3 = obj.pop_front()
# leetcode submit region end(Prohibit modification and deletion)

a=MaxQueue()
print(a.push_back(1))
print(a.push_back(2))
print(a.max_value())
print(a.pop_front())
print(a.max_value())
