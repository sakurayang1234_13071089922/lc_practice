# 给定一个整数数组 temperatures ，表示每天的温度，返回一个数组 answer ，其中 answer[i] 是指对于第 i 天，下一个更高温度出现
# 在几天后。如果气温在这之后都不会升高，请在该位置用 0 来代替。 
# 
#  
# 
#  示例 1: 
# 
#  
# 输入: temperatures = [73,74,75,71,69,72,76,73]
# 输出: [1,1,4,2,1,1,0,0]
#  
# 
#  示例 2: 
# 
#  
# 输入: temperatures = [30,40,50,60]
# 输出: [1,1,1,0]
#  
# 
#  示例 3: 
# 
#  
# 输入: temperatures = [30,60,90]
# 输出: [1,1,0] 
# 
#  
# 
#  提示： 
# 
#  
#  1 <= temperatures.length <= 10⁵ 
#  30 <= temperatures[i] <= 100 
#  
# 
#  Related Topics 栈 数组 单调栈 👍 1649 👎 0

from typing import List
# leetcode submit region begin(Prohibit modification and deletion)
class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        n = len(temperatures)
        
        result=[0]*n
        last_temp=None
        last_high_index=None
        for i in range(n):
            current_temperature=temperatures[i]
            if last_temp and current_temperature<=last_temp:
                last_temp = current_temperature
                
                for j in range(i+1,last_high_index+1):
                    if temperatures[j] > current_temperature:
                        result[i]=j-i
                        last_high_index = j
                        break
                continue
            if last_temp and current_temperature>last_temp:
                for j in range(last_high_index,n):
                    last_temp = current_temperature
                    
                    if temperatures[j] > current_temperature:
                        result[i] = j - i
                        last_high_index = j
                        break
                continue
                    
            for j in range(i+1,n):
                if temperatures[j]>current_temperature:
                    result[i]=j-i
                    last_temp=current_temperature
                    last_high_index=j
                    break
        return result
        
    
# leetcode submit region end(Prohibit modification and deletion)


test=Solution()
print(test.dailyTemperatures([73, 74, 75, 71, 69, 72, 76, 73]))