# 给你一个二叉树的根节点 root。设根节点位于二叉树的第 1 层，而根节点的子节点位于第 2 层，依此类推。 
# 
#  请返回层内元素之和 最大 的那几层（可能只有一层）的层号，并返回其中 最小 的那个。 
# 
#  
# 
#  示例 1： 
# 
#  
# 
#  
# 输入：root = [1,7,0,7,-8,null,null]
# 输出：2
# 解释：
# 第 1 层各元素之和为 1，
# 第 2 层各元素之和为 7 + 0 = 7，
# 第 3 层各元素之和为 7 + -8 = -1，
# 所以我们返回第 2 层的层号，它的层内元素之和最大。
#  
# 
#  示例 2： 
# 
#  
# 输入：root = [989,null,10250,98693,-89388,null,null,null,-32127]
# 输出：2
#  
# 
#  
# 
#  提示： 
# 
#  
#  树中的节点数在
#  [1, 10⁴]范围内
#  
#  -10⁵ <= Node.val <= 10⁵ 
#  
# 
#  Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 125 👎 0


# leetcode submit region begin(Prohibit modification and deletion)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxLevelSum(self, root: Optional[TreeNode]) -> int:
        tmp_node_list1=[]
        tmp_node_list2=[]
        max_level_val=1e-5
        result=0
        level_count=0
        tmp_node_list1.append(root)
        while (tmp_node_list1 or tmp_node_list2):
            level_count+=1
            current_node_list=tmp_node_list1 if tmp_node_list1 else tmp_node_list2
            next_node_list=tmp_node_list2 if tmp_node_list1 else tmp_node_list1
            current_level_sum=0
            for node in current_node_list:
                if not node:
                    continue
                current_level_sum+=node.val
                next_node_list.append(node.left)
                next_node_list.append(node.right)
            current_node_list.clear()
            if current_level_sum>max_level_val:
                result=level_count
                max_level_val=current_level_sum
            
        return result
        
        
# leetcode submit region end(Prohibit modification and deletion)
