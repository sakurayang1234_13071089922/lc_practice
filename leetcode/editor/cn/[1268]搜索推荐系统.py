# 给你一个产品数组 products 和一个字符串 searchWord ，products 数组中每个产品都是一个字符串。 
# 
#  请你设计一个推荐系统，在依次输入单词 searchWord 的每一个字母后，推荐 products 数组中前缀与 searchWord 相同的最多三个产品
# 。如果前缀相同的可推荐产品超过三个，请按字典序返回最小的三个。 
# 
#  请你以二维列表的形式，返回在输入 searchWord 每个字母后相应的推荐产品的列表。 
# 
#  
# 
#  示例 1： 
# 
#  输入：products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord 
# = "mouse"
# 输出：[
# ["mobile","moneypot","monitor"],
# ["mobile","moneypot","monitor"],
# ["mouse","mousepad"],
# ["mouse","mousepad"],
# ["mouse","mousepad"]
# ]
# 解释：按字典序排序后的产品列表是 ["mobile","moneypot","monitor","mouse","mousepad"]
# 输入 m 和 mo，由于所有产品的前缀都相同，所以系统返回字典序最小的三个产品 ["mobile","moneypot","monitor"]
# 输入 mou， mous 和 mouse 后系统都返回 ["mouse","mousepad"]
#  
# 
#  示例 2： 
# 
#  输入：products = ["havana"], searchWord = "havana"
# 输出：[["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
#  
# 
#  示例 3： 
# 
#  输入：products = ["bags","baggage","banner","box","cloths"], searchWord = 
# "bags"
# 输出：[["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"]
# ,["bags"]]
#  
# 
#  示例 4： 
# 
#  输入：products = ["havana"], searchWord = "tatiana"
# 输出：[[],[],[],[],[],[],[]]
#  
# 
#  
# 
#  提示： 
# 
#  
#  1 <= products.length <= 1000 
#  1 <= Σ products[i].length <= 2 * 10^4 
#  products[i] 中所有的字符都是小写英文字母。 
#  1 <= searchWord.length <= 1000 
#  searchWord 中所有字符都是小写英文字母。 
#  
# 
#  Related Topics 字典树 数组 字符串 二分查找 排序 堆（优先队列） 👍 164 👎 0
from collections import deque
from typing import List

# leetcode submit region begin(Prohibit modification and deletion)

class Trie:
    
    def __init__(self):
        # self.dict_tree = {}
        self.list_tree=[None]*26
        self.end = False
    
    def insert(self, word: str) -> None:
        node = self
        for i in word:
            _index=ord(i)-ord('a')
            if node.list_tree[_index]:
                node=node.list_tree[_index]
                continue
            node.list_tree[_index] = Trie()
            node=node.list_tree[_index]
            # if i in node.dict_tree:
            #     node = node.dict_tree[i]
            #     continue
            # node.dict_tree[i] = Trie()
            # node = node.dict_tree[i]
            
        node.end = True
        return
    
    def search(self, word: str) -> bool:
        node = self
        for i in word:
            _index=ord(i)-ord('a')
            
            # if i not in node.dict_tree:
            #     return False
            # node = node.dict_tree[i]
            
            if not node.list_tree[_index]:
                return False
            node=node.list_tree[_index]
        if node.end:
            return True
        return False
    
    def startsWith(self, prefix: str) -> bool:
        node = self
        for i in prefix:
            _index=ord(i)-ord('a')
            
            # if i not in node.dict_tree:
            #     return False
            # node = node.dict_tree[i]
            
            if not node.list_tree[_index]:
                return False
            node=node.list_tree[_index]
            
        return True
    
    def find_prefix(self, prefix: str):
        node = self
        for i in prefix:
            _index = ord(i) - ord('a')
            
            if not node.list_tree[_index]:
                return False
            node = node.list_tree[_index]
        
        return node
        

class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        dict_tree=Trie()
        for product in products:
            dict_tree.insert(product)
        result=[]
        n= len(searchWord)
        for i in range(n):

            prefix_node=dict_tree.find_prefix(searchWord[:i+1])
            if not prefix_node:
                result.append([])
                continue
            tmp_result=[]
            self.recursion(prefix_node,tmp_result,searchWord[:i+1])
            result.append(tmp_result)


        return result
    def recursion(self,node:Trie,result:List,node_char:str):
        if len(result)>=3:
            return
        if node.end:
            result.append(node_char)
        for index_,v in enumerate(node.list_tree):
            if not v:
                continue
            new_node_char=node_char+chr(index_+ord('a'))
            self.recursion(v,result,new_node_char)


# leetcode submit region end(Prohibit modification and deletion)
test=Solution()
print(test.suggestedProducts(products=["mobile", "mouse", "moneypot", "monitor", "mousepad"], searchWord= "mouse"))