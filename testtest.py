def solution(n):
    dp = [0] * n
    dp[0] = 1
    dp[1] = 1
    
    def _dp(n):
        if dp[n] != 0:
            return dp[n]
        dp[n] = _dp(n - 1) + _dp(n - 2)
        return dp[n]
    
    _dp(n - 1)
    return dp[n - 1]


_dp(5)
_dp(4) + _dp(3)
_dp(3) + _dp(2) + _dp(2) + _dp(1)
dp(2) + _dp(1) + _dp(2) + _dp(2) + _dp(1)
5 - 12

_dp(6)
_dp(5) + _dp(4)
dp(2) + _dp(1) + _dp(2) + _dp(2) + _dp(1) + dp(2) + _dp(1) + _dp(2)
6 - 14


def solution2(n):
    a, b = 1, 1
    if n <= 2:
        return 1
    for i in range(n - 2):
        a, b = b, a + b
    return b


print(solution2(3))
print(solution2(4))
print(solution2(5))
print(solution(30))

'''
address
id        address   tag   delete_at
address1  aaa  version1
address1  bbb  version2   2023.12.19
address2  ccc  version1

order
user id  address id
user1  address2 ...

'''

'''

def process(a,b):

'''
