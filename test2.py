import time
from threading import Thread


def timeit(max_time):
    def decorator(func):
        def wrap(*args, **kwargs):
            start_time = time.time()
            result = func(*args, **kwargs)
            end_time = time.time()
            a = func.__name__
            execution_time = end_time - start_time
            print("{} function ran for  {}s".format(a, execution_time))
            if execution_time > max_time:
                print("{} function exceeds max_time".format(a))
            return result

        return wrap

    return decorator


@timeit(max_time=1)
def tes123t(test):
    time.sleep(2)
    # print(test)
    return test


class SharedKls:
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
            return cls.instance
        else:
            return cls.instance


class CustomDict:
    def __setitem__(self, key, value):

        self.__setattr__(str(hash(key)), value)

    def __getitem__(self, key):
        if str(hash(key)) in self.__dict__:
            return self.__dict__[str(hash(key))]
        else:
            raise KeyError


def thread_print():
    def print_(num):
        for i in range(1001):
            if i % 10 == num:
                print("thread-{}: {}".format(num, i))

    for i in range(10):
        t = Thread(target=print_, args=(i,))
        t.start()


from typing import List


class Solution:
    def find_lowest_players(self, k: int, map: List[int]) -> List[int]:
        result = []
        # for i in range(len(map)):
        #     if (i+k-1)<len(map):
        #         print(i,i+k-1)
        #         result.append(min(map[i:i+k]))
        # return result
        # [1, 2, 6, 2, 9, 6, 1, 8]

    def find_lowest_players_2(self, k: int, map: List[int]) -> List[int]:
        result = []
        for i, v in enumerate(map):
            if len(result) < i + 1:
                result.insert(i, v)
            for j in range(max(0, i - 2), i):
                if result[j] > v:
                    result[j] = v
        return result[0:len(map) - k + 1]


def can_win(gem_cost: list[int], damage: list[int], total_gems: int, hp:
int) -> bool:
    if sum(damage) < hp:
        return False

    for i in range(len(gem_cost)):
        damage_sum = damage[i]
        gem_cost_sum = gem_cost[i]
        for j in range(i + 1, len(gem_cost)):
            damage_sum += damage[j]
            gem_cost_sum += gem_cost[j]
            if damage_sum >= hp:
                if gem_cost_sum <= total_gems:
                    return True

    return False


if __name__ == '__main__':
    # assert can_win([1, 2, 3, 4, 5], [2, 4, 6, 8, 10], 10, 20) == True
    # assert can_win([1, 5], [7, 6], 10, 15) == False
    # assert can_win([1, 3, 5, 7, 20, 30, 35], [2, 6, 8, 12, 20, 30, 40], 40, 48) == True
    # assert can_win([1, 3, 5, 7, 20, 30, 35], [2, 6, 8, 12, 20, 30, 40], 40, 49) == False
    # print(tes123t("test"))
    dict_=CustomDict()
    dict_["test"]=1
    print(dict_["test"])