class max_heaq:
    def __init__(self, iterable):
        self.max_heaq = [0] * len(iterable)
        self.new_index = 0
        for i in iterable:
            self.insert(i)
    
    def insert(self, val):
        new_index = self.new_index
        self.new_index += 1
        self.max_heaq[new_index] = val
        if new_index == 0:
            return
        parent_index = (new_index - 1) // 2
        while self.max_heaq[parent_index] < self.max_heaq[new_index] and new_index > 0:
            self.max_heaq[parent_index], self.max_heaq[new_index] = self.max_heaq[new_index], self.max_heaq[
                parent_index]
            new_index = parent_index
            parent_index = (new_index - 1) // 2
    
    def pop(self):
        delete_index = 0
        pop_val = self.max_heaq[delete_index]
        self.max_heaq[delete_index] = self.max_heaq.pop()
        self.new_index -= 1
        left_index = delete_index * 2 + 1
        right_index = delete_index * 2 + 2
        while (left_index < self.new_index) and (right_index < self.new_index):
            if self.max_heaq[delete_index] < self.max_heaq[left_index] or self.max_heaq[delete_index] < self.max_heaq[
                right_index]:
                if self.max_heaq[left_index] > self.max_heaq[right_index]:
                    self.max_heaq[delete_index], self.max_heaq[left_index] = self.max_heaq[left_index], self.max_heaq[
                        delete_index]
                    delete_index = left_index
                else:
                    self.max_heaq[delete_index], self.max_heaq[right_index] = self.max_heaq[right_index], self.max_heaq[
                        delete_index]
                    delete_index = right_index
                left_index = delete_index * 2 + 1
                right_index = delete_index * 2 + 2
            else:
                break
        if left_index < self.new_index and self.max_heaq[left_index] > self.max_heaq[delete_index]:
            self.max_heaq[delete_index], self.max_heaq[left_index] = self.max_heaq[left_index], self.max_heaq[
                delete_index]
        return pop_val
    
    def head(self):
        if self.new_index > 0:
            return self.max_heaq[0]
        return None


def tester(nums, k):
    print(len(nums))
    test = max_heaq(nums)
    print(test.max_heaq)
    for i in range(k - 1):
        test.pop()
    print(test.head())
    print(test.max_heaq)
    return test.head()

# test = max_heaq([-1, 2, 0])
# test = max_heaq([3, 2, 1, 5, 6, 4])
# test = max_heaq([3, 2, 3, 1, 2, 4, 5, 5, 6])
# print(test.max_heaq)
# print(test.head())
# test.pop()
# print(test.max_heaq)
# print(test.head())
# tester([3, 2, 3, 1, 2, 4, 5, 5, 6], 4)
# tester([1, 2, 3, 4, 5, 6], 2)
# tester([2, 3, 4, 5, 6], 2)
# tester([-1, -2, -3, -1, -2, -4, -5, -5, -6], 4)
