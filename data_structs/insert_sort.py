"""
    insert sort algorithm in python

"""


def insert_sort(unsorted_list: list) -> list:
    # length = len(unsorted_list)
    # for i in range(1, length):
    #     key = unsorted_list[i]
    #     j = i - 1
    #     while j >= 0 and unsorted_list[j] > key:
    #         unsorted_list[j + 1] = unsorted_list[j]
    #         j -= 1
    #     unsorted_list[j + 1] = key
    # return unsorted_list

    # 实现2 从左到右插入排序
    length = len(unsorted_list)
    sorted_key = 1
    for i in range(1, length):
        for j in range(sorted_key):
            key = unsorted_list[i]
            if key < unsorted_list[j]:
                unsorted_list[i] = unsorted_list[j]
                unsorted_list[j] = key
        sorted_key += 1
    return unsorted_list


print(insert_sort([0, 3, 1, 5, 1, 5, 8, 3, 4]))
