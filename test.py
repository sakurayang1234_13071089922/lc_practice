import sys

# inputs = [i for i in sys.stdin]
# inputs = ['a5\n']
# inputs = ['abcdef\n']
# inputs = ['abC124ACb\n']
# inputs = ['aBB9\n']
# inputs = ['765427\n']
inputs = ['765427a69a4753\n']
origin_str = inputs[0].replace("\n", "")
max_length = 0
origin_str_list = [i for i in origin_str]
# print(origin_str_list)
n = len(origin_str_list)
# print(n)
max_sub_num_length = 0
sub_num_range_list = []
for i, v in enumerate(origin_str_list):
    sub_str_length = 1
    range_right = i
    if v.isnumeric():
        for j in range(i + 1, n):
            if origin_str_list[j].isnumeric():
                sub_str_length += 1
                range_right = j
                continue
            break
        sub_num_range_list.append([i, range_right])
        max_sub_num_length = max(max_sub_num_length, sub_str_length)
# print(sub_num_range_list)
# print(len(sub_num_range_list))
if not max_sub_num_length:
    max_length = -1
elif max_sub_num_length == n:
    max_length = -1
else:
    for i, v in enumerate(sub_num_range_list):
        if i == len(sub_num_range_list) - 1:
            continue
        
        for j in range(i + 1, n):
            if j == len(sub_num_range_list) - 1:
                break
            if v[1] >= sub_num_range_list[j][0]:
                continue
            elif v[1] == sub_num_range_list[j][0] - 2:
                real_length = sub_num_range_list[j][1] - v[0] + 1
                max_length = max(real_length, max_length)
                break
    
    max_length = max(max_length, max_sub_num_length + 1)

print(max_length)
